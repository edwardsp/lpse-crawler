module Lpse
end

require 'nokogiri'
require 'sequel'
require 'logger'
require 'httparty'
require 'pry'
require 'csv'
require_relative './multi-io'

class Lpse::ListGrabber

  def grab
    raw_page = HTTParty.get("https://inaproc.lkpp.go.id/v3/daftar_lpse", verify: false)
    html_page = Nokogiri::HTML(raw_page)

    data = []

    rows = html_page.css("tbody").css("tr")
    rows.each do |row|
      cols = row.css("td")
      url = cols[1].css("a")[0]["href"].strip
      unless url == ""
        uri = URI.parse(url)
        unless uri.scheme
          uri = URI.parse("http://" + url)
        end
        data << {
          kode: cols[0].text,
          nama: cols[1].text.gsub(/^LPSE/, "").strip,
          url: uri.scheme + "://" + uri.host
        }
      end
    end

    data
  end
end