module Lpse
end

require 'nokogiri'
require 'logger'
require 'httparty'
require 'pry'
require 'retriable'
require_relative './multi-io'

class Lpse::Crawler
  PROCUREMENT_LIST="%{base_url}/eproc/lelang/pemenangcari.gridtable.pager/%{page}"
  PROCUREMENT_DETAIL="%{base_url}/eproc/lelang/view/%{id}"
  PROCUREMENT_PEMENANG="%{base_url}/eproc/lelang/pemenang/%{id}"

  def initialize(kode, nama, url)
    # @logger = Logger.new MultiIO.new(STDOUT, File.open("lpse.log", "a"))
    @logger = Logger.new MultiIO.new(File.open("lpse.log", "a"))
    @institution = {
      kode: kode,
      nama: nama,
      url: url,
      created_at: Time.now,
      updated_at: Time.now
    }
    @last_page = 1
  end

  def run(db)
    @db = db
    begin
      process_list_page
      if @last_page > 1
        for page in 2..@last_page
          process_list_page(page)
        end
      end
    rescue Exception => e
      @logger.error e
    end
  end

  def process_list_page(page = 1)
    raw_page = request(procurement_list_url(page))
    html_page = Nokogiri::HTML(raw_page)

    if page == 1 then
      institution = load_institution(@institution[:url])
      save_institution(@institution) unless institution and institution[:id]

      @logger.debug "Detecting last page..."
      pager = html_page.css("a[href^='/eproc/lelang/pemenangcari.gridtable.pager/']")
      @last_page = pager.empty? ? 1 : pager.last.text.to_i
      @logger.debug "Last page is #{@last_page}"
    end

    procurement_ids = []
    html_page.css("a[href^='/eproc/lelang/view/']").each do |link|
      if link["href"] =~ /\/eproc\/lelang\/view\/(\d+)/ then
        procurement_ids << $1.to_i
      end
    end
    procurement_ids.uniq!

    procurement_ids.each do |procurement_id|
      process_procurement_page(procurement_id)
    end
  end

  def process_procurement_page(procurement_id)
    begin
      raw_page = request(procurement_detail_url(procurement_id))
      html_page = Nokogiri::HTML(raw_page)

      @logger.debug find_value_table(html_page, "Kode Lelang")

      data = {
        institution_id: @institution[:id],
        kode: find_value_table(html_page, "Kode Lelang"),
        nama: find_value_table(html_page, "Nama Lelang"),
        satuan_kerja: find_value_table(html_page, "Satuan Kerja"),
        kategori: find_value_table(html_page, "Kategori"),
        metode_pengadaan: find_value_table(html_page, "Metode Pengadaan"),
        metode_kualifikasi: find_value_table(html_page, "Metode Kualifikasi"),
        metode_dokumen: find_value_table(html_page, "Metode Dokumen"),
        metode_evaluasi: find_value_table(html_page, "Metode Evaluasi"),
        anggaran: find_value_table(html_page, "Anggaran"),
        harga_pagu: convert_to_numeric(find_value_table(html_page, "Nilai Pagu Paket")),
        hps: convert_to_numeric(find_value_table(html_page, "Nilai HPS Paket")),
        kualifikasi_usaha: find_value_table(html_page, "Kualifikasi Usaha"),
        lokasi: find_value_table(html_page, "Lokasi Pekerjaan"),
        created_at: Time.now,
        updated_at: Time.now
      }
      procurement = load_procurement(data[:kode])
      unless procurement
        procurement = save_procurement(data)
        process_pemenang_page(procurement)
      end      
    rescue Exception => e
      @logger.error e
    end
  end

  def process_pemenang_page(procurement)
    raw_page = request(procurement_pemenang_url(procurement[:kode]))
    html_page = Nokogiri::HTML(raw_page)

    tables = html_page.css('table')
    if tables and tables.length == 2
      rows = tables[1].css("tr")

      col_nama = 1
      col_nama = false
      col_administrasi = false
      col_teknis = false
      col_harga_penawaran = false
      col_harga_terkoreksi = false
      col_pemenang = false

      rows[0].css("th").each_with_index do |c, i|
        case c.text.strip
        when "Nama Peserta"
          col_nama = i
        when "Administrasi"     
          col_administrasi = i
        when "Teknis"
          col_teknis = i
        when "Harga Penawaran"
          col_harga_penawaran = i
        when "Harga Terkoreksi"
          col_harga_terkoreksi = i
        when "Pemenang"
          col_pemenang = i
        end
      end

      rows.each_with_index do |row, i|
        next if i == 0
        cols = row.children
        cols[col_nama].text =~ /(.*) - ([0-9A-Za-z.-]+)$/
        data = {
          nama: $1,
          npwp: $2,
          created_at: Time.now,
          updated_at: Time.now
        }

        participant = load_participant(data[:npwp])
        unless participant
          participant = save_participant(data)
        end

        data = {
          procurement_id: procurement[:id],
          participant_id: participant[:id],
          created_at: Time.now,
          updated_at: Time.now
        }
        data[:administrasi] = cols[col_administrasi].css("img").any? if col_administrasi
        data[:teknis] = cols[col_teknis].css("img").any? if col_teknis
        data[:harga_penawaran] = convert_to_numeric(cols[col_harga_penawaran].text) if col_harga_penawaran
        data[:harga_terkoreksi] = convert_to_numeric(cols[col_harga_terkoreksi].text) if col_harga_terkoreksi
        data[:pemenang] = cols[col_pemenang].css("img").any? if col_pemenang

        save_procurement_participant(data)
      end

      update_alamat(find_value_table(tables[0], "NPWP"), find_value_table(tables[0], "Alamat"))
    end
  end




  def load_institution(url)
    @logger.debug "Find institution: #{url}..."
    data = @db[:institutions].where(url: url).first
    if data
      @institution = data 
      @logger.debug "Institution found: #{@institution[:nama]}"
    end
    @institution
  end

  def save_institution(data)
    @logger.debug "Saving institution..."
    id = @db[:institutions].insert(data)
    @logger.debug "Institution is saved with id #{id}..."
    return load_institution(data[:url])
  end




  def load_procurement(kode)
    @logger.debug "Finding procurement with kode: #{kode}..."
    data = @db[:procurements].where(kode: kode).first
  end

  def save_procurement(data)
    @logger.debug "Saving procurement #{data[:kode]}..."
    id = @db[:procurements].insert(data)
    @logger.debug "Procurement is saved with id #{id}."
    return load_procurement(data[:kode])
  end




  def load_participant(npwp)
    @logger.debug "Finding participant with NPWP: #{npwp}..."
    data = @db[:participants].where(npwp: npwp).first
  end

  def save_participant(data)
    @logger.debug "Saving participant #{data[:npwp]}..."
    id = @db[:participants].insert(data)
    @logger.debug "Participant is saved with id #{id}."
    return load_participant(data[:npwp])
  end

  def update_alamat(npwp, alamat)
    @logger.debug "Update alamat participant #{npwp}..."
    data = @db[:participants].where(npwp: npwp).update(alamat: alamat)
  end





  def load_procurement_participant(id)
    data = @db[:procurement_participants].where(id: id).first
  end

  def save_procurement_participant(data)
    @logger.debug "Saving procurement #{data[:procurement_id]} with participant #{data[:participant_id]}..."
    id = @db[:procurement_participants].insert(data)
    @logger.debug "Procurement participant is saved with id #{id}."
    return load_procurement_participant(id)
  end



  private

  def procurement_list_url(page)
    PROCUREMENT_LIST % {
      base_url: @institution[:url],
      page: page
    }
  end

  def procurement_detail_url(procurement_id)
    PROCUREMENT_DETAIL % {
      base_url: @institution[:url],
      id: procurement_id
    }
  end

  def procurement_pemenang_url(procurement_id)
    PROCUREMENT_PEMENANG % {
      base_url: @institution[:url],
      id: procurement_id
    }
  end

  def find_value_table(html_page, title)
    col = html_page.search("td[normalize-space()='#{title}']")
    if col[0]
      return col[0].next.text
    end
    return nil
  end

  def convert_to_numeric(value)
    value = value.gsub(/[^\d\,]/, '')
    return nil if (value == "")
    value.to_f
  end

  def request(url)
    Retriable.retriable timeout: 120, on: [Errno::ECONNREFUSED, SocketError, Net::ReadTimeout] do |try|
      @logger.debug("Request: #{url}")
      return HTTParty.get(url, verify: false)
    end
  end
end